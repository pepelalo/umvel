# Título del Proyecto

_Prueba para vacante FrontEnd para la empresa Umvel, Administrador de usuarios_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de evaluacion._

```
 Clonar el repositorio para poder bajar el codigo del proyecto con el siguiente comando:
 git clone https://pepelalo@bitbucket.org/pepelalo/umvel.git
```

### Pre-requisitos 📋

_Para poder ejecutar este proyecto en tu maquina local necesitaremos instalar NodeJs y Angular cli, no te preocupes, aqui encontraras las ligas para poder descargar e instalar en tu maquina ;)

```
 GIT: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
 NodeJs: https://nodejs.org/es/
 Angular: npm install -g @angular/cli
```
_Para la versión de Node se recomienda la LTS, en  git los pasos de instalación se describen en la pagina dependiendo de tu sistema operativo
### Instalación 🔧

_Una vez que tengamos el repositorio clonado en nuestro entorno local, procedemos a los siguiente pasos:

_Entrar a la carpeta donde se encuentra en el proyecto y ejecutar el comando npm install, este comando nos ayudara a instalar las dependencias necesarias que necesita nuestra aplicación para poder levantar nuestro servidor

_Una vez concluido la instalación de los paquetes procederemos a levantar nuestro servidor, con el siguiente comando: ng serve

_Ejemplo: suponiendo que mi proyecto se encuentra en Documentos/umvel, abrimos una terminar y ejecutamos lo siguiente

```
 $ cd Documentos/umvel
 $ npm install
 $ng serve
  .. ....
  .....
  http://localhost:4200
```
_cuando el servidor se levanto correctamente podemos abrir nuestro navegador y entrar a la siguiente direccion http://localhost:4200

## Construido con 🛠️

_El siguiente proyecto fue construido con las siguientes tecnologias.

* [Angular](https://angular.io/cli) - El framework web usado
* [NodeJs](https://nodejs.org/es//) - 
* [Angular Material](https://material.angular.io/e/) - Usado para usar componentes UI
* [Boostrap](https://getbootstrap.com/) - Usado como framework para la parte de los styles
* [CokieeService](https://www.npmjs.com/package/ngx-cookie-service) - Usado para la gestion de los datos en las cookies


## Autores ✒️

_José Eduardo Sánchez Méndez_

* **José Eduardo Sánchez Méndez** - *Angular Developers* - [pepelalo](https://bitbucket.org/pepelalo/)


