import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../../../services/login.service";
import {CookieService} from "ngx-cookie-service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm!: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private cookieService: CookieService,
    private router: Router
  ) {
     this.createForm();
  }

  ngOnInit(): void {

  }

  createForm () {
    this.loginForm = this.formBuilder.group( {
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  submited() {
    this.loginService.singIn(this.loginForm.value).subscribe((valor: any) => this.setAutenticationToken(valor.token));
  }

  setAutenticationToken(token: string) {
    this.cookieService.set('token', token);
    this.router.navigate(['/app']);
  }

}
