import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  opened = true;
  constructor() { }

  ngOnInit(): void {
  }

  onOpenMenu() {
    this.opened = true;
  }

  onToggleNavbar() {
    this.opened = !this.opened;
  }

}
