import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { UsersListComponent } from './components/users.-listcomponent';
import { UsersContainerComponent } from './container/users-container/users-container.component';
import { UsersService } from 'src/app/services/users.service';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { UsersPostsComponent } from './components/users-posts/users-posts.component';

/** ANGULAR MATERIAL */
import {MatCardModule} from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatDialogModule} from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
@NgModule({
  declarations: [
    UsersListComponent,
    UsersContainerComponent,
    UsersDetailComponent,
    UsersPostsComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatDividerModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatExpansionModule,
    MatIconModule
  ],
  providers: [UsersService],
  entryComponents: [UsersDetailComponent]
})
export class UsersModule { }
