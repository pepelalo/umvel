import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPost } from '../../interfaces/post.interface';

@Component({
  selector: 'app-users-posts',
  templateUrl: './users-posts.component.html',
  styleUrls: ['./users-posts.component.scss']
})
export class UsersPostsComponent implements OnInit {
  panelOpenState = false;
  @Input() posts: IPost[];
  @Output() deletePost = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

}
