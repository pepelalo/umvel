import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';

import { IResponseUsers } from '../interfaces/user.interface';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  @Input() responseUsers: IResponseUsers;
  @ViewChild('paginator') paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'first_name', 'last_name','email'];
  @Output() detail = new EventEmitter();
  @Output() changePage = new EventEmitter();
  constructor() { }

  ngOnInit(): void {


  }

  userDetail(id) {
    this.detail.emit(id);
  }


}
