import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsersService } from 'src/app/services/users.service';
import { IPost } from '../../interfaces/post.interface';
import { IUser, IuserPosts } from '../../interfaces/user.interface';


@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss']
})
export class UsersDetailComponent implements OnInit {
  form: FormGroup;
  constructor(
    private formBulder: FormBuilder,
    public dialogRef: MatDialogRef<UsersDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IuserPosts,
    private userService: UsersService
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.form.patchValue(this.data.user);
  }

  createForm() {
    this.form = this.formBulder.group({
      id: ['',Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      avatar: ['']
    })
  }
  onClose(): void {
    this.dialogRef.close();
  }
  submited() {
    this.userService.updateUserId(this.form.value).subscribe(() => this.onClose());
  }

  deletePost(id: number) {
    this.userService.deletePostId(id).subscribe((data) => {
    })
  }

}
