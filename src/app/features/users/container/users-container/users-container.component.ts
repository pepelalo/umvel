import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';
import { UsersDetailComponent } from '../../components/users-detail/users-detail.component';
import { IPost } from '../../interfaces/post.interface';
import { IPage, IResponseUsers, IUser, IUserDetailResponse } from '../../interfaces/user.interface';

@Component({
  selector: 'app-users-container',
  templateUrl: './users-container.component.html',
  styleUrls: ['./users-container.component.scss']
})
export class UsersContainerComponent implements OnInit {
  usersList$: BehaviorSubject<IResponseUsers> = new BehaviorSubject(null);
  constructor(
    private usersServices: UsersService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(page?: number) {
    this.usersServices.getUsers(page).subscribe((data) => this.usersList$.next(data));
  }
  userDetail(id: number)
  {
    const user$ = this.usersServices.getUserId(id);
    const posts$ = this.usersServices.getPostUserId(id);
    const usersPost$ = combineLatest([user$, posts$]).subscribe(([user, posts]) => {
      this.openDialog(user.data, posts);
    })
  }
  openDialog(user: IUser, posts: IPost[]): void {
    const dialogRef = this.dialog.open(UsersDetailComponent, {
      maxHeight:'90vh',
      data: {
        user,
        posts
      }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
  changePage(page: IPage) {
    this.getUsers(page.pageIndex +1);
  }

}
