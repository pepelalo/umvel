import { IPost } from "./post.interface";

export interface IUser {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export interface IResponseUsers {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: IUser[]
}

export interface IUserDetailResponse {
  data: IUser,
  support: {
    url: string,
    text: string

  }
}

export interface IPage {
  previousPageIndex:number,
  pageIndex:number,
  pageSize:number,
  length:12
}

export interface IuserPosts {
  posts: IPost[],
  user: IUser
}
