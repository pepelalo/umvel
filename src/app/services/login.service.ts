import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = environment.api_host;
  constructor(
    private http: HttpClient
  ) { }

  singIn(user: any) {
    const body = {
      email: user.email,
      password: user.password
    }
    return this.http.post(this.url + 'login' ,body);
  }
}
