import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { IPost } from '../features/users/interfaces/post.interface';
import { IResponseUsers, IUser, IUserDetailResponse } from '../features/users/interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  url = environment.api_host;
  url_post = environment.api_post;
  opened = new BehaviorSubject(false);
  constructor(
    private http: HttpClient
  ) { }

  getUsers(page = 1): Observable<IResponseUsers> {
    let params = new HttpParams().set('page', page);
    return this.http.get<IResponseUsers>(`${this.url}users`, { params })
  }

  getUserId(id) {
    return this.http.get<IUserDetailResponse>(`${this.url}users/${id}`)
  }
  updateUserId(user: IUser){
    return this.http.put(`${this.url}users/${user.id}`, {user})
  }

  getPostUserId(userId: number) {
    let params = new HttpParams().set('userId', userId);
    return this.http.get<IPost[]>(`${this.url_post}posts`, { params })
  }

  deletePostId (id: number) {
    return this.http.delete(`${this.url_post}posts/${id}`);
  }
}
