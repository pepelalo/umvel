import { Route } from '@angular/compiler/src/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NavItem } from '../interfaces/nav-item.interface';

@Component({
  selector: 'app-sidenav-item',
  templateUrl: './sidenav-item.component.html',
  styleUrls: ['./sidenav-item.component.scss']
})
export class SidenavItemComponent implements OnInit {
  @Input() depth!: number;
  @Input() menuOpened!: boolean;
  @Output() openMenuEmitter = new EventEmitter<void>();
  item = {
    displayName: 'Users',
    display: false,
    enabled: true,
    iconName: 'user',
  };
  constructor(
    private route: Router
  ) { }

  ngOnInit(): void {
  }
  toggleMenu(): void {
  }

}
