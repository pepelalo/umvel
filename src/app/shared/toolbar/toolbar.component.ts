import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input() logoTemplate!: TemplateRef<any>;
  @Input() menuDitoTemplate!: TemplateRef<any>;
  @Output() toggleNavbar = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

}
