import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { NavItem } from '../interfaces/nav-item.interface';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @Input() opened = true;
  @Output() openMenuEmitter = new EventEmitter<void>();

  constructor() {
   }

  ngOnInit(): void {
  }

  toggleMenu(): void {
    this.opened = !this.opened;
  }

}
