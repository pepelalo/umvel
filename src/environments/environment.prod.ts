export const environment = {
  production: true,
  api_host: 'https://reqres.in/api/',
  api_post: 'https://jsonplaceholder.typicode.com/'
};
